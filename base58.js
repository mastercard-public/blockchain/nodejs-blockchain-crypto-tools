const bs58 = require('bs58');

module.exports.decodeRaw = function(data) {
    return bs58.decode(data);
}

module.exports.encode = function(data) {
    return bs58.encode(new Buffer(data));
}

module.exports.decode = function(data) {
    return bs58.decode(data).toString();
}