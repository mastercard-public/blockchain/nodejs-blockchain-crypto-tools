var aes = require('../nodejs-blockchain-crypto-tools').aes();
const crypto = require('crypto');

beforeEach(() => {
  //NOOP
});

afterEach(() => {
  //NOOP
});


test('check aes', () => {
  var key = crypto.randomBytes(32);
  var iv = crypto.randomBytes(16);
  var data = "some very loooooooonnnnnnnnnnggggg text";
  var cipherText = aes.encrypt(data, key, iv, 'base64');
  var plainText = aes.decrypt(cipherText, key, iv, 'base64', 'utf8');
  expect(plainText).toBe(data);
});

