var hash = require('../nodejs-blockchain-crypto-tools').hash();

beforeEach(() => {
    //NOOP
});

afterEach(() => {
    //NOOP
});

test('check sha1 hash', () => {
    expect(hash.sha1('hello, the world!')).toBe('07473759FC3DAB4BD2E298AFA945BA69D0634919');
});

test('check sha256 hash', () => {
    expect(hash.sha256('hello, the world!')).toBe('A77E028E5AC3ED17BC19B8B8C7F0934433481A0E70179A85C0F911F3B9246117');
});
