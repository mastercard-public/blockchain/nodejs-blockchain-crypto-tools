var chain = require('../nodejs-blockchain-crypto-tools').chain();
var pki = require('../nodejs-blockchain-crypto-tools').pki();
const crypto = require('crypto');

beforeEach(() => {
  //NOOP
});

afterEach(() => {
  //NOOP
});


test('check address generation', () => {
  var address = chain.generateAddress(chain.addressPrefix.entity);
  expect(chain.verifyAddress(address.id, address.publicKey)).toBeTruthy();
  var data = "Some Text !";
  var signature = pki.sign(data, address.privateKeyPem);
  expect(pki.verify(data, signature, address.publicKeyPem)).toBeTruthy();
});

