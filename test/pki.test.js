var pki = require('../nodejs-blockchain-crypto-tools').pki();

beforeEach(() => {
  //NOOP
});

afterEach(() => {
  //NOOP
});


test('check pki sign/verify', () => {
  var keyPair = pki.generate();
  var data = "some data !";
  var signed = pki.sign(data, keyPair.privatePem);
  expect(pki.verify(data, signed, keyPair.publicPem)).toBeTruthy();
});

test('check pki secret', () => {
  var keyPairA = pki.generate();
  var keyPairB = pki.generate();
  expect(pki.computeSecret(keyPairA.private, keyPairB.public)).toBe(pki.computeSecret(keyPairB.private, keyPairA.public));
});

