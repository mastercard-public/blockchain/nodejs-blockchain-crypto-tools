var base58 = require('../nodejs-blockchain-crypto-tools').base58();

beforeEach(() => {
    //NOOP
});

afterEach(() => {
    //NOOP
});

test('check encode', () => {
    expect(base58.encode('Sample Data')).toBe('MgE3Ux3sDXWTGak');
});

test('check decode', () => {
    expect(base58.decode('MgE3Ux3sDXWTGak')).toBe('Sample Data');
});
