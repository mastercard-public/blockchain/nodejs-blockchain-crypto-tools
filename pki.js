const crypto = require('crypto'), ec_pem = require('ec-pem'), base58 = require('./base58');

module.exports.generate = function () {
  var curve = 'prime256v1';
  var ecdh = crypto.createECDH(curve);
  ecdh.generateKeys();
  var pem = ec_pem(ecdh, curve);
  return {
    privatePem: pem.encodePrivateKey(),
    publicPem: pem.encodePublicKey(), 
    private: ecdh.getPrivateKey(), 
    public: ecdh.getPublicKey(), 
    privateRaw: Array.prototype.slice.call(ecdh.getPrivateKey(), 0), 
    publicRaw: Array.prototype.slice.call(ecdh.getPublicKey(), 0)
  };
}

module.exports.sign = function (data, key) {
  const sign = crypto.createSign('SHA256');
  sign.write(data);
  sign.end();
  return sign.sign(key);
}

module.exports.verify = function (data, signature, key) {
  const verify = crypto.createVerify('SHA256');
  verify.write(data);
  verify.end();
  return verify.verify(key, signature);
}

module.exports.computeSecret = function (privateKey, publicKey) {
  var curve = 'prime256v1';
  var ecdh = crypto.createECDH(curve);
  ecdh.setPrivateKey(privateKey);
  var secret = ecdh.computeSecret(publicKey);
  return secret.toString('hex');
}

