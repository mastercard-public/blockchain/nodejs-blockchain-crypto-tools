const crypto = require('crypto');

function hash(data, algo, encode) {
    var hash = crypto.createHash(algo);
    hash.update(Array.isArray(data) ? new Buffer(data) : data);
    return encode ? hash.digest().toString('hex').toUpperCase() : hash.digest();
}

module.exports.hash = hash;

module.exports.sha1 = function (data) {
    return hash(data, 'sha1', true);
}

module.exports.sha256 = function (data) {
    return hash(data, 'sha256', true);
}

