# NodeJs Crypto SDK for Mastercard Blockchain

### Introduction
This project implements cryptography module specific for Mastercard Blockchain.

### Build
Run command: 
   * `npm install`

### Test
Run command:
   * `npm test` 
 
