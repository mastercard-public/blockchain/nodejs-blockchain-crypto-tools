const crypto = require('crypto');
const algorithm = 'aes-256-cfb';

module.exports.encrypt = function (data, key, iv, encoding) {
  var encryptor;
  encryptor = crypto.createCipheriv(algorithm, key, iv);
  encryptor.setEncoding(encoding);
  encryptor.write(data);
  encryptor.end();
  return encryptor.read();
}

module.exports.decrypt = function (data, key, iv, inEncoding, outEncoding) {
  const decryptor = crypto.createDecipheriv(algorithm, key, iv);
  decryptor.setEncoding(outEncoding);
  decryptor.write(data, inEncoding);
  decryptor.end();
  return decryptor.read();
}
