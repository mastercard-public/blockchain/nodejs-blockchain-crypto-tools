const crypto = require('crypto'), pki = require('./pki'), hash = require('./hash'), base58 = require('./base58');


module.exports.privateKey = 128;

module.exports.addressPrefix = {
  alpha: 23,
  personal: 55,
  device: 30,
  entity: 33,
  application: 23,
  consensus: 28,
  signatory: 63,
  audit: 80,
  witness: 50
};


function arrayEquals(arrA, arrB) {
  return (arrA.length == arrB.length && arrA.every(function (itm, idx) {
    return itm === arrB[idx];
  }));
}

module.exports.hash160 = function (data) {
  return Array.prototype.slice.call(hash.hash(data, 'sha1', false), 0);
};

module.exports.checksum = function (data) {
  var hashed = hash.hash(Array.isArray(data) ? new Buffer(data) : data, 'sha256', false);
  return Array.prototype.slice.call(hashed, 0, 4);
};

module.exports.encodeAddress = function (prefix, data) {
  var hashed = module.exports.hash160(data);
  var keyHash = [prefix].concat(hashed);
  var checksum = module.exports.checksum(keyHash);
  return base58.encode(keyHash.concat(checksum));
};

module.exports.decodeAddress = function (data) {
  return Array.prototype.slice.call(base58.decodeRaw(data), 1, 21);
};

module.exports.encodeWif = function (data) {
  var keyHash = [module.exports.privateKey].concat(data);
  var checksum = module.exports.checksum(keyHash);
  return base58.encode(keyHash.concat(checksum));
};

module.exports.generateAddress = function (prefix) {
  var keyPair = pki.generate();
  return {
    id: module.exports.encodeAddress(prefix, keyPair.publicRaw),
    wif: module.exports.encodeWif(keyPair.privateRaw),
    publicKey: keyPair.publicRaw,
    privateKey: keyPair.privateRaw,
    publicKeyPem: keyPair.publicPem,
    privateKeyPem: keyPair.privatePem
  };
};

module.exports.verifyAddress = function (address, key) {
  return arrayEquals(module.exports.decodeAddress(address), module.exports.hash160(key));
};

module.exports.verify = function (type) {
};

